<?php

include_once "services/service.php";
include_once "business/usersManager.php";


class AccountService extends Service {
    function run(){

        $this->usersManager = new UsersManager($this->config, $this->dbManager);
        
        
        
        switch($this->data['service']){
            case "checkEmail":
                $this->checkEmail();
                break;
            case "checkPassword" :
                $this->checkPassword();
                break;
        }

    }
    
    
    function checkEmail(){
        
        
        if($this->usersManager->checkUserExistence($this->inputData))
            print "taken";
        else
            print "free";
    }
    
    function checkPassword(){
        
        
        if($this->usersManager->checkUserCredentials($this->inputData))
            print "OK";
        else 
            print "NOK";
    }
}