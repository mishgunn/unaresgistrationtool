<?php

include_once "services/service.php";

include_once "business/dataStorage.php";
include_once "business/usersManager.php";
include_once "business/mailSender.php";
include_once "business/dataTemplater.php";
include_once "business/pdfGenerator.php";

class MemberService extends Service{

    function validate(){
        return ($this->inputData && $this->config && $this->dbManager);
    }
    
    function run(){

        $config = $this->config;
        $lPostData = $this->inputData;
        
        $dataStorage = new DataStorage($this->config, $this->dbManager);
        $usersManager = new UsersManager($this->config, $this->dbManager);
        $mailSender = new MailSender($this->config);
        $dataTemplater = new DataTemplater($this->config);
        $pdfGenerator = new PdfGenerator($this->config);

        

        $lMessagesUrl = "../shared/data/messages.json";

        $lMessages = json_decode(file_get_contents($lMessagesUrl));

        if($lPostData->category->code == "school"){
            $lPostData->confirmationTitle = $lMessages->legends->parentPermission;
            $lPostData->confirmationContent = $lMessages->info->parentPermission;
        }else{
            $lPostData->confirmationTitle = $lMessages->legends->honnorDeclaration;
            $lPostData->confirmationContent = $lMessages->info->honnorDeclaration;
        }

        $lStoredData = $lPostData;
        $lStoredData->dump = $this->rawInput;
        $lPostData->regNumber = date("Ymd")."01000";

        $lUserId = $usersManager->checkUserExistence($lPostData);
        
        if(!$lUserId){
            
            $lUserId = $usersManager->saveUser($lPostData);
        }else {
        
            $lUserId = $usersManager->checkUserCredentials($lPostData);
        }
        
        if($lUserId){
            
            $lMemberInsertId = $dataStorage->storeMemberData($lStoredData);
            if($lMemberInsertId > 0){
                $lPostData->regNumber = substr_replace($lPostData->regNumber, $lMemberInsertId, -strlen($lMemberInsertId));
                $usersManager->linkMember($lUserId, $lMemberInsertId);
            }
        }else{
            print "NOK";
            return false;
        }

        $lData = array('data' => $lPostData, 'messages'=> $lMessages, 'config'=>$config );
        $lPdfContent = $dataTemplater->render("registrationForm.html", $lData);
        $lMailContent = $dataTemplater->render("mailNotification.html", $lData);
        $lAccountMailContent = $dataTemplater->render("mailAccountCreation.html", $lData);

        $lParams['title'] = $lMessages->mail->registration->title;
        $lParams['message'] = $lMailContent;
        $lParams['sender']['email'] = $config['mail'];
        $lParams['sender']['name'] = $config['senderName'];
        $lParams['addressee']['email'] = $lPostData->email;
        $lParams['addressee']['name'] = $lPostData->name . " " . $lPostData->surname;
        
        $lFileName = $config['root']."../storage/registrationForm_".$lPostData->name."_".$lPostData->surname."_".mktime().".pdf";

        $pdfGenerator->generatePdf($lPdfContent, $lFileName);

        $lParams['filePath'][] = $lFileName;

        
        
        $lRegConfirmMsg = $mailSender->generateMessage($lParams);

        
        

        $lAdminParams = $lParams;
        $lAdminParams['title'] = "[UNA] New member registered";
        $lAdminParams['message'] = "New member registered";
        $lAdminParams['addressee']['email'] = $config['adminmail'];
        $lAdminParams['addressee']['name'] = $config['adminmail'];
        $lAdminMsg = $mailSender->generateMessage($lAdminParams);

        
        $lAccountMailParams['title'] = $lMessages->mail->account->title;
        $lAccountMailParams['message'] = $lAccountMailContent;
        $lAccountMailParams['sender']['email'] = $config['mail'];
        $lAccountMailParams['sender']['name'] = $config['senderName'];
        $lAccountMailParams['addressee']['email'] = $lPostData->email;
        $lAccountMailParams['addressee']['name'] = $lPostData->name . " " . $lPostData->surname;
        
        
        $lAccountConfirmMsg = $mailSender->generateMessage($lAccountMailParams);
        
        if($lRegConfirmMsg){
            $mailSender->sendMessage($lAdminMsg);
            
            $mailSender->sendMessage($lAccountConfirmMsg);
            
            $mailSender->sendMessage($lRegConfirmMsg);
            print "OK";
        }
        else{
            return failure();
        }
    }
}