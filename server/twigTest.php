<?php
require_once 'modules/Twig/lib/Twig/Autoloader.php';

$lDataInput = file_get_contents("php://input");
$lData = json_decode($lDataInput);
Twig_Autoloader::register();



$lLoader = new Twig_Loader_Filesystem('./templates/');
$lTwig = new Twig_Environment($lLoader);


$lMessagesUrl = "../shared/data/messages.json";

$lMessages = json_decode(file_get_contents($lMessagesUrl));

if($lData->category->code == "scolar"){
    $lData->confirmationTitle = $lMessages->legends->parentPermission;
    $lData->confirmationContent = htmlspecialchars_decode($lMessages->info->parentPermission);
}else{
    $lData->confirmationTitle = $lMessages->legends->honnorDeclaration;
    $lData->confirmationContent = htmlspecialchars_decode($lMessages->info->honnorDeclaration);
}

echo $lTwig->render('registrationForm.html', array('data' => $lData, 'messages'=> $lMessages));