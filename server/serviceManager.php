<?php

include_once "config/config.php";
include_once "business/dbManager.php";
include_once "services/accountService.php";
include_once "services/memberService.php";

$dbManager = new DBManager($config);

$service = null;
switch($_GET['service']){
    case "checkEmail" :
    case "checkPassword" :
        
        $service = new AccountService($config, $dbManager, $_GET);
        
        break;
    case "saveForm" :
        $service = new MemberService($config, $dbManager, null);
        break;
}

if($service->validate())
    $service->run();
else
    $service->failure();
?>