<?php

class UsersManager {

    function __construct($config, $pDBManager){
        if($pDBManager != null)
            $this->dbManager = $pDBManager;
    }

    function saveUser($pData){

        if($this->dbManager && $stmt = $this->dbManager->connection -> prepare("
                insert into una_custom_users
                (name, surname, login, email, password)
                values (?, ?, ?, ?, ?)")) {

                $stmt -> bind_param("sssss",
                        trim($pData->name) ,
                        trim($pData->surname),
                        trim($pData->email),
                        trim($pData->email),
                        md5(trim($pData->password)));

                /* execute query */
                $stmt -> execute();
                $userId = $stmt->insert_id;
                $stmt -> close();
                if($userId)
                    return $userId;
                else
                    return 0;

        }
        return 0;
    }

    function checkUserExistence($pData){
        if($this->dbManager){
            if($stmt = $this->dbManager->connection -> prepare("
                    select id from una_custom_users
                    where login = ?")){
                    $stmt -> bind_param("s",
                            trim($pData->email));
                    $stmt -> execute();
                    $stmt->bind_result($userId);
                    $stmt->fetch();
                    $stmt -> close();
                    if($userId)
                        return $userId;
                    else
                        return 0;
            }
        }
        return 0;
    }

    function checkUserCredentials($pData){
        if($this->dbManager){
            if($stmt = $this->dbManager->connection -> prepare("
                    select id, name, surname, login, email, password from una_custom_users
                    where login = ?")){
                    $stmt -> bind_param("s",
                            trim($pData->email));
                    $stmt -> execute();

                    $stmt->bind_result($userId, $userName, $userSurname, $userLogin, $userEmail, $userPassword);
                    $stmt->fetch();
                    $user['id'] = $userId;
                    $user['name'] = $userName;
                    $user['surname'] = $userSurname;
                    $user['login'] = $userLogin;
                    $user['email'] = $userEmail;
                    $user['password'] = $userPassword;

                    $stmt -> close();
                    if($user['id'] && $this->checkPassword($pData, $user)){
                        return $user['id'];
                    }
                    else
                        return 0;
            }
        }
        return 0;
    }

    function checkPassword($pData, $pUser){
        $lResult = false;
        if($pData->encrypt){
            $lResult = $pData->password == $pUser['password'];
        }else{
            $lResult = md5($pData->password) == $pUser['password'];
        }


        return $lResult;
    }

    function linkMember($pUserId, $pMemberId){
        
        if($this->dbManager && ($pMemberId > 0 && $pUserId > 0)){
        
            if($stmt = $this->dbManager->connection -> prepare("
                    insert into una_custom_join_users_members
                    (user_id, member_id)
                    values (?, ?)")){
                    $stmt -> bind_param("ii",
                            $pUserId, $pMemberId);
                    $stmt -> execute();
                    $linkId = $stmt->insert_id;
                    $stmt -> close();
                    if($linkId)
                        return true;
                    else
                        return false;

            }
        }

        return false;
    }
}