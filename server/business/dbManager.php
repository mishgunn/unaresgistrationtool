<?php
class DBManager{
    function __construct($pConfig){
        $this->connection = new mysqli($pConfig['host'], $pConfig['user'], $pConfig['password']);

        //$selected = mysqli_select_db($db, $conf -> database);
        $this->dbSelection = $this->connection->select_db($pConfig['database']);

        if (!$this->dbSelection) {
            die ('Cannot use DB : '.mysqli_error($this->connection));
        }
        if (mysqli_connect_errno()) {
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }

        if (!$this->connection)
            die('Failed to connect to the database. Please retry the request later.');
    }
}