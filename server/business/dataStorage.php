<?php

class DataStorage {
    function __construct($pConfig, $pDBManager){
        if($pDBManager != null)
            $this->dbManager = $pDBManager;
        
        
    }

    function storeMemberData($pData){
        
        
        if($this->dbManager && $stmt = $this->dbManager->connection -> prepare("
                insert into una_custom_members
                (name, surname, licence_number,
                email, birthdate, dump)
                values (?, ?, ?, ?, ?, ?)")) {

            $pData->birthdate = DataProcessor::formatDate($pData->birthdate);
            
            $stmt -> bind_param("ssssss",
                    trim($pData->name),
                    trim($pData->surname),
                    trim($pData->licenceNo),
                    trim($pData->email),
                    $pData->birthdate,
                    $pData->dump);
    
            /* execute query */
            $stmt -> execute();
            $memberId = $stmt->insert_id;
            $stmt -> close();
            if($memberId)
                return $memberId;
            else 
                return 0;
        }
    }
}
